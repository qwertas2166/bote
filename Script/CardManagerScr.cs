using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public struct Card
{
    public string Name;
    public Sprite Logo;
    public int Attack, Defence;

    public Card(string name, string logoPath, int attack, int defence)
    {
        Name = name;
        Logo = Resources.Load<Sprite>(logoPath);
        Attack = attack;
        Defence = defence;
    }
}

public static class CardManager
{
    public static List<Card> AllCards = new List<Card>(); 
}

public class CardManagerScr : MonoBehaviour
{

    public void Awake()
    {
        CardManager.AllCards.Add(new Card("faet", "Sprites/faet", 5, 5));
        CardManager.AllCards.Add(new Card("finodiri", "Sprites/finodiri", 4, 3));
        CardManager.AllCards.Add(new Card("kornevik", "Sprites/kornevik", 3, 3));
        CardManager.AllCards.Add(new Card("legard", "Sprites/legard", 2, 1));
        CardManager.AllCards.Add(new Card("danu", "Sprites/danu", 8, 1));
        CardManager.AllCards.Add(new Card("ursun", "Sprites/ursun", 1, 1));
    }

}