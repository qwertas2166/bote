using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class Game
{
    public List<Card> EnemyDeck, PlayerDeck,
                      EnemyHand, PlayerHand,
                      EnemyField, PlayerField;

    public Game()
    {
        EnemyDeck = GiveDeckCard();
        PlayerDeck = GiveDeckCard();

        EnemyHand = new List<Card>();
        PlayerHand = new List<Card>();

        EnemyField = new List<Card>();
        PlayerField = new List<Card>();
    }

    List<Card> GiveDeckCard()
    {
        List<Card> list = new List<Card>();
        for (int i = 0; i < 10; i++)
            list.Add(CardManager.AllCards[Random.Range(0, CardManager.AllCards.Count)]);
        return list;
    }
}

public class GameManagerScr : MonoBehaviour {

    private const string CARD_PREF_PATH = "Prefab/CardPref";
    // private const string LOGO_PREF_PATH = "Prefab/Logo";
    // private const string NAME_PREF_PATH = "Prefab/Name";
    
    public Game CurrentGame;
    public Transform EnemyHand, PlayerHand;
    public GameObject CardPref;

    void Start ()
    {
        CurrentGame = new Game();

        GiveHandCards(CurrentGame.EnemyDeck, EnemyHand);
        GiveHandCards(CurrentGame.PlayerDeck, PlayerHand);

    }

    // private void GiveCardToHand(List<Card> enemyDeck, Transform enemyHand)
    // {
    //     throw new System.NotImplementedException();
    // }

    void GiveHandCards(List<Card> deck, Transform hand)
    {
        int i = 0;
        while (i++ < 4)
            GiveCardToHand(deck, hand);
    }

    void GiveCardToHand(List<Card> deck, Transform hand)
    {
        if (deck.Count == 0)
            return;

        Card card = deck[0];
        
        GameObject CardGO = Instantiate(Resources.Load<GameObject>(CARD_PREF_PATH), hand, false);
        GameObject logoGO = CardGO.transform.Find("Logo").gameObject;
        GameObject nameGO = CardGO.transform.Find("Name").gameObject;

        if (hand == EnemyHand)
            CardGO.GetComponent<CardInfoScr>().HideCardInfo(card, logoGO, nameGO);
        else
            CardGO.GetComponent<CardInfoScr>().ShowCardInfo(card, logoGO, nameGO);

        deck.RemoveAt(0);
    }
}
